#include <iostream>
#include <string>

#include "client/Client.h"

int main(int argc, char **argv) {
    std::string host = "localhost";
    int port = 0;
    if (argc == 1) {
        std::cout << "Invalid usage! See use -h to see all options" << std::endl;
        return 1;
    }

    for (int i = 1; i < argc; i ++) {
        if (std::string(argv[i]) == "-h") {
            std::cout << "Attributes:\n    -h <host> : set host. Default value - 'localhost'\n    -p <port> : set port" << std::endl;
            return 0;
        }
        else if (std::string(argv[i]) == "-p") {
            i++;
            port = std::atoi(argv[2]);
        }
        else if (std::string(argv[i]) == "-h") {
            i++;
            host = argv[i];
        }
    }

    if (!port) {
        std::cout << "Port value not specified. Use '-p <port>' option" << std::endl;
        return 1;
    }

    std::string username;
    std::cout << "Enter username: ";
    getline(std::cin, username);
    std::cout << std::endl;

    Client client(host, port);
    client.setUsername(username);
    std::string message;
    while (true) {
        std::cout << "Enter message: ";
        getline(std::cin, message);
        if (!client.sendMessage(message)) {
            std::cout << "Something goes wrong. Try again!" << std::endl;
        }
    }
}

//int port = 0;
//if (argc == 1) {
//port = 9876;
//std::cout << "Using default port: " << port << std::endl;
//}
//else if (argc == 3) {
//if (std::string(argv[1]) == "-p") {
//port = std::atoi(argv[2]);
//if (port) {
//std::cout << "Using provided port: " << port << std::endl;
//}
//}
//}
//
//if (port == 0) {
//std::cout << "Invalid arguments provided. You can specify port value by using '-p <port>' option." << std::endl;
//return 1;
//}
