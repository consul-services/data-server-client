#ifndef CONSUL_SERVICES_CLIENT_H
#define CONSUL_SERVICES_CLIENT_H

#include <memory>
#include <string>

#include <cpp-httplib/httplib.h>

class Client {
public:
    Client(const std::string &host, int port);
    Client() = delete;

    bool sendMessage(const std::string &message);
    bool setUsername(const std::string &username);

private:
    std::unique_ptr<httplib::Client> client;

    std::string username;
};
#endif //CONSUL_SERVICES_CLIENT_H
