#include "Client.h"

#include <nlohmann/json.hpp>

Client::Client(const std::string &host, int port)
{
    client = std::make_unique<httplib::Client>(host, port);
}

bool Client::sendMessage(const std::string &message) {
    if (!username.empty() && !message.empty()) {
        httplib::Request request;

        nlohmann::json json;
        json["username"] = username;
        json["message"] = message;

        httplib::Result result = client->Post("/message", json.dump(), "application/json");
        std::cout << "Responce: " << result.value().body << std::endl << std::endl;

        return result.error() == httplib::Error::Success;
    }
    return false;
}

bool Client::setUsername(const std::string &name) {
    username = name;
    return !username.empty();
}
